-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

CREATE DATABASE `uas_18020057` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `uas_18020057`;

DROP TABLE IF EXISTS `pegawai`;
CREATE TABLE `pegawai` (
  `nip` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tmplahir` varchar(50) NOT NULL,
  `tgllahir` date NOT NULL,
  `jenis` varchar(10) NOT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `pegawai` (`nip`, `nama`, `tmplahir`, `tgllahir`, `jenis`) VALUES
('12312312312',	'Humaedi',	'Tangerang',	'2023-01-26',	'L'),
('12312431230923',	'Rohan',	'Pontianak',	'1998-11-11',	'L'),
('1231243123123',	'Ahmad',	'Jakarta',	'1999-04-05',	'L'),
('1238192383232',	'Fitri',	'Banten',	'2000-06-17',	'P');

-- 2023-01-28 10:57:55
